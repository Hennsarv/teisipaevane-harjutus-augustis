﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InimeseAken
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TetsipäevaneÜlesanne.Program.Main(null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label1.Text =
            TetsipäevaneÜlesanne.Inimene.Nimekiri[int.Parse(this.textBox1.Text)].ToString();
        }
    }
}
