esmasp�evane (teisip�evane) �lesanne:

tee programm, milles on �ks klass
ntx Inimene v�i �pilane v�i Student - vali ise

tal v�iks olla sellised asjad
Eesnimi kontrollitud v�li e property
Perenimi --,,--
T�isnimi readonly property 
S�nniaeg datetime kontrollitud v�li
Vanus int readonly property
NB! Nimed asjadele m�tle ise v�lja

reeglid
nimed on suure algust�hega
s�nniaeg peab olema t�nasest mitte vanem kui 80 aastat
ja mitte hilisem kui t�nane
vigane s�nniaeg asenda t�nasega

Katseta Mainis erinevaid asju, kas toimib
Ahjah - sellel asjal v�iks ka ToString olla - vahvam ju

T�IENDAME

Klassis peaks olema nimekiri k�igist inimestest
Igal inimesel v�iks olla unikaalne j�rjekorranumber
Nimekiri v�iks olla koostatud Dictionarina

TEIE T�IENDATE OMA KLASSI

1. lisage oma projekti �ks fail kus on inimeste andmed
	kujul
	Henn, Sarv, 7.3.1955
	...
2. t�iendage oma Maini v�i lisage Inimesele staatiline meetod
	mis loeb sellest failist inimesed
3. Proovige need inimesed siis ka v�lja printida

