﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetsipäevaneÜlesanne
{
    public class Program
    {
        static public void Main(string[] args)
        {
            Inimene.LoeInimesed();

            Inimene[] inimesed =
            {
                new Inimene {EesNimi ="Henn", PereNimi="sarv", SünniAeg = new DateTime(1955,3,7)},
                new Inimene {EesNimi ="Toomas", PereNimi="Linnupoeg", SünniAeg = DateTime.Parse("3.7.1980")},
                new Inimene {EesNimi ="Arno", PereNimi="TALI", SünniAeg = DateTime.Parse("3.8.1900")},
                new Inimene {EesNimi ="Ants", PereNimi="SAUNAmees", SünniAeg = DateTime.Parse("3.9.2000")}

            };
            foreach (Inimene i in Inimene.Nimekiri.Values) Console.WriteLine(i);

            //inimesed[0].TäisNimi = "sarvik taat";
            //Console.WriteLine(inimesed[0].TäisNimi);
            //Console.WriteLine(inimesed[3].Vanus);

            //Console.Write("Anna number: ");
            //Console.WriteLine(Inimene.Nimekiri[int.Parse(Console.ReadLine())]);

        }
    }

    public class Inimene
    {
        //see on nimekiri, kuhu pannakse kõik inimesed
        public static Dictionary<int, Inimene> Nimekiri = new Dictionary<int, Inimene>();
        
        
        // eemaldasin muutuja _Loendur ja asendasin AutoPropertyga
        // mis on public loetav ja private muudetav
        public static int Loendur { get; private set; } = 0;

        private static readonly string FileName = @"..\..\Inimesed.txt";

        private int _Nr; public int Nr => _Nr;
        
        private string _EesNimi;
        private string _PereNimi;
        private DateTime _SünniAeg;

        public Inimene()
        {
            _Nr = ++Loendur;
            Nimekiri.Add(_Nr, this);
        }


        public string EesNimi  // property Eesnimi on kirjutatud pikalt
        {
            get { return _EesNimi; }
            set
            {
                _EesNimi = value == "" ? value :
              value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
            }
        }

        public string PereNimi // property perenimi on kirjutatud lühikeselt
        {
            get => _PereNimi;
            set => _PereNimi = value == "" ? value : value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
        }

        public string TäisNimi
        {
            get
            {
                return _EesNimi + (_PereNimi == "" ? "" : " " + _PereNimi);
            }
            set {
                string[] osad = (value+" ").Split(' ');
                EesNimi = osad[0];
                PereNimi = osad[1];
            }
        }


        public DateTime SünniAeg
        {
            get => _SünniAeg;
            set => _SünniAeg = 
                (value > DateTime.Now || value < DateTime.Now.AddYears(-80)) 
                ? DateTime.Today : value;
        }

        public int Vanus
        {
            get
            {
                int vanus = DateTime.Now.Year - _SünniAeg.Year;
                if (_SünniAeg.AddYears(vanus) > DateTime.Today) vanus--;
                return vanus;
            }
        }

        public override string ToString()
        => $"{_Nr}. {TäisNimi} sündinud {SünniAeg.ToShortDateString()} ({Vanus} aastane)";

        public static void LoeInimesed2()
        {
            System.IO.File.ReadAllLines(FileName)
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(x => new Inimene { EesNimi = x[0], PereNimi = x[1], SünniAeg = DateTime.Parse(x[2]) })
                .ToList();
        }

        public static void LoeInimesed()
        {
            string[] read = System.IO.File.ReadAllLines(FileName);
            foreach(string rida in read)
            {
                string[] osad = (rida + ",,").Split(',');
                for (int i = 0; i < osad.Length; i++) osad[i] = osad[i].Trim();
                if (DateTime.TryParse(osad[2], out DateTime sünniaeg))
                {
                    new Inimene { EesNimi = osad[0], PereNimi = osad[1], SünniAeg = sünniaeg };
                }
            }
        }

    }

    class Account
    {
        double Amount = 0;
        readonly double Rate = 0;

        public void Credit(double amount) => Amount -= amount;
        public void Debit(double amount) => Amount += amount;
        
        public void AddRate() => Amount *= (1+Rate);

        public Account()
        {
            Rate = 0.01;
        }
        public Account(double initialRate)
        {
            Rate = initialRate;
        }


    }
}
